const axios = require('axios')

class API {
    constructor() {
        this._URL = 'https://economia.awesomeapi.com.br/json/all'
        this._METHOD = 'GET'
    }

    async data() {
        return await axios.default.get(this._URL, { method: this._METHOD })
            .then(response => response.data)
    }
}

module.exports = new API()