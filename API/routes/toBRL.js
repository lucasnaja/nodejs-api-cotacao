module.exports = async (app, data) => {
    let URLS = '<h1>TO BRL</h1><p>Use ?amount=número para mudar o valor, ex: /USD?amount=10</p>'
    const names = Object.keys(data)

    names.map((name, index) => {
        URLS += `<a href="/${name}?amount=1"><p style="margin:0">/${name} - ${data[name]['name']} para BRL</p><a/><br>`
        app.get(`/${name}`, (req, res) => {
            const amount = req.query.amount ? req.query.amount : 1
            res.status(200).send(`${amount} ${name} = ${amount * parseFloat(data[name]['high'].replace(',', '.'))} BRL`)
        })
    })

    return URLS
}