module.exports = async (app, API) => {
    const data = await API.data()
    let URL = ''

    URL = await require('./toBRL')(app, data)
    URL += await require('./BRLto')(app, data)

    return URL
}