module.exports = async (app, data) => {
    let URLS = '<h1>BRL TO</h1><p>Use ?amount=número para mudar o valor, ex: /BRL/USD?amount=10</p>'
    const names = Object.keys(data)

    names.map((name, index) => {
        URLS += `<a href="/BRL/${name}?amount=1"><p style="margin:0">/BRL/${name} - BRL para ${data[name]['name']}</p><a/><br>`
        app.get(`/BRL/${name}`, (req, res) => {
            const amount = req.query.amount ? req.query.amount : 1
            res.status(200).send(`${amount} BRL = ${amount / parseFloat(data[name]['high'].replace(',', '.'))} ${name}`)
        })
    })

    return URLS
}