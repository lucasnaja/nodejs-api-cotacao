const app = require('express')()
const API = require('./API/API')
const { _PORT } = require('./API/configs/global')

require('./API/routes')(app, API)
    .then(URL => {
        app.get('/', (req, res) => res.status(200).send(URL))

        app.listen(_PORT, () => console.log('initialized'))
    })

